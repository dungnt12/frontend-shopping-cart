<?php
    $method = $_SERVER['REQUEST_METHOD'];
    $target_file = '';
    if ($method === 'POST' && isset($_FILES["fileToUpload"])) {
      $TARGET_DIR = "../assets/";
      $target_file = $TARGET_DIR . basename($_FILES["fileToUpload"]["name"]);
      $uploadOk = 1;
      $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
      $err = '';
    
      // Check if image file is a actual image or fake image
      $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
      if($check !== false) {
        $err = "File is an image - " . $check["mime"] . ".";
        $uploadOk = 1;
      } else {
        $err = "File is not an image.";
        $uploadOk = 0;
      }
    
      // Check if file already exists
      if (file_exists($target_file)) {
        $err = "Sorry, file already exists.";
        $uploadOk = 0;
      }
    
      // Check file size
      if ($_FILES["fileToUpload"]["size"] > 10 * 1024 * 1024) {
        $err = "Sorry, your file is too large.";
        $uploadOk = 0;
      }
    
      // Allow certain file formats
      if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
      && $imageFileType != "gif" && $imageFileType != "webp") {
        $err = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
        $uploadOk = 0;
      }
    
      // Check if $uploadOk is set to 0 by an error
      if ($uploadOk == 0) {
        $err = "Sorry, your file was not uploaded.";
      // if everything is ok, try to upload file
      } else {
        if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
          $err = "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
        } else {
          $err = "Sorry, there was an error uploading your file.";
        }
      }
    }
?>