class Login {
  constructor() {
    this.init();
  }

  init() {
    const loginBtn = qs('#login-model');
    loginBtn.onclick = (e) => {
      e.preventDefault();
      const loginForm = {
        username: qs('#usernameLogin').value,
        password: qs('#passwordLogin').value,
      }

      this.error = [];
      this._validate(loginForm);

      if (this.error.length) {
        this.toast = _createToast('Error', this.error, 'bg-warning');
        document.body.appendChild(this.toast);
        $(`#${this.toast.id}`).toast('show');
      }

      if (!this.error.length) {
        $.ajax({
          type: "POST",
          url: "server/api/user.php",
          data: {
            username: loginForm.username,
            password: loginForm.password,
            type: 'login'
          },
          success: (res) => {

            localStorage.setItem('login', JSON.stringify(res));
            if (res.role === 'admin') {
              window.location.replace("admin/product");
              return;
            }

            validate.init();
            $('#login').modal('hide');
            this.toast = _createToast('Success!', 'Đăng nhập thành công, giờ bạn có thể mua hàng !', 'bg-success');
            document.body.appendChild(this.toast);
            $(`#${this.toast.id}`).toast('show');
          }
        });
      }
    }
  }

  _validate = (loginForm) => {
    Object.keys(loginForm).forEach((key) => {
      const value = loginForm[key];
      if (!value.trim()) {
        this.error.push(`${key} không được để trống`);
      }
    });
  }
}

const login = new Login();