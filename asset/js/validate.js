const navbarBeforeLogin = `
<nav class="navbar navbar-expand-lg navbar-light bg-light">
<a class="navbar-brand" href="#">Trang chủ</a>
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
  aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
  <span class="navbar-toggler-icon"></span>
</button>

<div class="collapse navbar-collapse" id="navbarSupportedContent">
  <ul class="navbar-nav ml-auto">
    <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
        aria-haspopup="true" aria-expanded="false">
        Dịch vụ
      </a>
      <div class="dropdown-menu" aria-labelledby="navbarDropdown">
        <a class="dropdown-item" href="#" data-toggle="modal" data-target="#register">Đăng kí</a>
        <a class="dropdown-item" href="#" data-toggle="modal" data-target="#login">Đăng nhập</a>
      </div>
    </li>
  </ul>
  <form class="form-inline my-3 my-lg-0">
    <input class="form-control mr-sm-3 searchProduct" type="search" placeholder="Hãy thử tìm một vài sản phẩm"
      aria-label="Search">
  </form>
</div>
</nav>
`;

const navbarAfterLogin = (username) => `
<nav class="navbar navbar-expand-lg navbar-light bg-light">
<a class="navbar-brand" href="#">Trang chủ</a>
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
  aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
  <span class="navbar-toggler-icon"></span>
</button>

<div class="collapse navbar-collapse" id="navbarSupportedContent">
  <ul class="navbar-nav ml-auto">
    <li class="nav-item">
      <a class="nav-link cartMenu" href="#" data-toggle="modal" data-target="#cart">Sản phẩm</a>
    </li>
    <li class="nav-item logout">
      <a class="nav-link" href="#">Chào ${username}</a>
    </li>
  </ul>
  <form class="form-inline my-3 my-lg-0">
    <input class="form-control mr-sm-3 searchProduct" type="search" placeholder="Hãy thử tìm một vài sản phẩm"
      aria-label="Search">
  </form>
</div>
</nav>
`;

class Validate {
  constructor() {
    this.init();
  }

  init() {
    const dataLocalstorage = localStorage.getItem('login');
    const navbarWrapper = document.querySelector('#navbar-auth');
    try {
      const dataPaser = JSON.parse(dataLocalstorage);
      if (dataPaser) {
        navbarWrapper.innerHTML = navbarAfterLogin(dataPaser.username);
      } else {
        navbarWrapper.innerHTML = navbarBeforeLogin;
      }

      navbarWrapper.querySelector('.cartMenu').onclick = () => {
        cart._getCart();
      }
    } catch (error) {
      console.log(error);
    }
  }
}

const validate = new Validate();