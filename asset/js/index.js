﻿const qs = (el) => document.querySelector(el);

Element.prototype.setAttributes = function (attrs) {
  for (var idx in attrs) {
    if ((idx === 'styles' || idx === 'style') && typeof attrs[idx] === 'object') {
      for (var prop in attrs[idx]) { this.style[prop] = attrs[idx][prop]; }
    } else if (idx === 'html') {
      this.innerHTML = attrs[idx];
    } else {
      this.setAttribute(idx, attrs[idx]);
    }
  }
};

const validateEmail = (email) => {
  const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}

const makeid = (length) => {
  var result = '';
  var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  var charactersLength = characters.length;
  for (var i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}


_createToast = (title, content, status) => {
  const toast = document.createElement('div');
  toast.setAttributes({
    'id': makeid(10),
    'class': `toast ${status}`,
    'role': 'alert',
    'aria-live': 'assertive',
    'aria-atomic': 'true',
    'data-delay': '2000',
    'style': 'position: fixed; top: 1rem; right: 1rem;',
  });
  toast.innerHTML = `
    <div class="toast-header">
      <strong class="mr-auto">${title}</strong>
      <small>just now</small>
      <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="toast-body">
      ${content instanceof Array ? content.map((err) => `${err}<br />`).join('\n') : content}
    </div>
  `;
  return toast;
}