// Handle for register
class Register {
  constructor() {
    this.registerBtn = qs('#registerBtn');
    this.error = [];
    this.init();
  }

  init() {
    qs('#registerBtn').onclick = this.register;
  }

  register = async (e) => {
    e.preventDefault();
    const toasts = document.querySelectorAll('.toast');
    // remove toast
    toasts.forEach((toast) => {
      document.body.removeChild(toast);
    });
    this.error = [];

    const registerForm = {
      username: qs('#usernameRegister').value,
      password: qs('#passwordRegister').value,
      rePassword: qs('#re-passwordRegister').value,
      email: qs('#emailRegister').value
    }

    this._validate(registerForm);

    if (this.error.length) {
      this.toast = _createToast('Error', this.error, 'bg-warning');
      document.body.appendChild(this.toast);
      $(`#${this.toast.id}`).toast('show');
    }

    if (!this.error.length) {
      $.ajax({
        type: "POST",
        url: "server/api/user.php",
        data: {
          username: registerForm.username,
          email: registerForm.email,
          password: registerForm.password,
          type: 'register'
        },
        success: (res) => {
          localStorage.setItem('login', JSON.stringify(res));
          validate.init();
          $('#register').modal('hide');
          this.toast = _createToast('Success!', 'Đăng kí thành công, giờ bạn có thể đăng nhập và mua hàng !', 'bg-success');
          document.body.appendChild(this.toast);
          $(`#${this.toast.id}`).toast('show');
        }
      });
    }
  }

  _validate = (registerForm) => {
    Object.keys(registerForm).forEach((key) => {
      const value = registerForm[key];
      if (!value.trim()) {
        this.error.push(`${key} không được để trống`);
      }

      switch (key) {
        case 'rePassword': {
          if (registerForm.password !== registerForm.rePassword) {
            this.error.push('Password không trùng khớp');
          }
          break;
        }

        case 'email': {
          if (!validateEmail(registerForm.email)) {
            this.error.push('Email Không hợp lệ');
          }
        }

          break;

        default:
          break;
      }
    });
  }
}

const register = new Register();