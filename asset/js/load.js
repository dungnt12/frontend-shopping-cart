class Load {
  constructor() {
    this.init();
  }

  init() {
    this._loadProduct();
  }

  _loadProduct() {
    document.addEventListener('readystatechange', () => {
      $.ajax({
        type: "GET",
        url: "server/api/product.php",
        success: (data) => {
          const productDOM = qs('#products-list');
          const dataProductDOM = data.map(({ description, id, images, price, title, type }) => (
            `
            <div class="col-sm-6 mb-2 productItem">
              <div class="card">
              <img class="card-img-top" src="server/${images}" alt="${title}">
              <div class="card-body">
                <h5 class="card-title">${title}</h5>
                <p class="card-text">${description}</p>
                <p class="card-text">${price}</p>
                <p class="card-text"><span class="badge badge-secondary typeProduct">${type}</span></p>
                <a href="#" class="btn btn-primary buynow" data-id="${id}">Buy now</a>
              </div>
              </div>
            </div>
          `
          ));
          productDOM.innerHTML = dataProductDOM.join('');
          const allBtnBuy = productDOM.querySelectorAll('.buynow');

          const findProduct = (idProduct) => {
            const index = data.findIndex(({ id }) => String(id) === idProduct);
            return data[index];
          };

          allBtnBuy.forEach((btn) => {
            btn.onclick = (e) => {
              e.preventDefault();
              const id = btn.getAttribute('data-id');
              const currentProduct = findProduct(id);
              this._addProductToLocalStorage(currentProduct);

              this.toast = _createToast('Success!', `Đã thêm sản phẩm ${currentProduct.title} vào giỏ hàng`, 'bg-success');
              document.body.appendChild(this.toast);
              $(`#${this.toast.id}`).toast('show');
            }
          });
          this._search();
          this._generatorList(data);
        }
      });
    })
  }

  _addProductToLocalStorage = (currentProduct) => {
    const productsLocalstorage = localStorage.getItem('productsBuy');
    try {
      const parseProduct = JSON.parse(productsLocalstorage);
      if (parseProduct) {
        const index = parseProduct.findIndex((item) => item.id === currentProduct.id);
        if (index > - 1) {
          const newCount = parseProduct[index].count + 1;
          parseProduct[index] = { ...currentProduct, count: newCount };
          localStorage.setItem('productsBuy', JSON.stringify(parseProduct));
        } else {
          localStorage.setItem('productsBuy', JSON.stringify(parseProduct.concat([{ ...currentProduct, count: 1 }])));
        }
      } else {
        localStorage.setItem('productsBuy', JSON.stringify([{ ...currentProduct, count: 1 }]));
      }
    } catch (error) {
      console.log(error);
    }
  }

  _search = () => {
    const productSearch = qs('.searchProduct');
    const products = document.querySelectorAll('.productItem');

    productSearch.onkeyup = (e) => {
      const value = e.target.value;

      products.forEach((product) => {
        product.style.display = 'block';
        if (!product.querySelector('.card-title').innerHTML.includes(value)) {
          product.style.display = 'none';
        }
      });
    };
  }

  // !TODO: click vào thì lấy ra sản phẩm tương ứng
  _generatorList = (data) => {
    const listGroupDOM = qs('.list-group');
    const count = this._getCount(data.map((e) => e.type));
    let contentDOM = '';
    Object.keys(count).forEach((key) => {
      contentDOM += `
      <li type-data="${key}" class="list-group-item d-flex justify-content-between align-items-center typeCount">
        ${key}
        <span class="badge badge-primary badge-pill">${count[key]}</span>
      </li>
      `
    });

    contentDOM += `
    <li type-data="all" class="list-group-item d-flex justify-content-between align-items-center typeCount">
      All
    </li>
    `;

    listGroupDOM.innerHTML = contentDOM;

    const products = document.querySelectorAll('.productItem');
    listGroupDOM.querySelectorAll('.typeCount').forEach((data) => {
      const type = data.getAttribute('type-data');
      data.onclick = () => {
        if (type === 'all') {
          products.forEach((product) => {
            product.style.display = 'block';
          });
        } else {
          products.forEach((product) => {
            product.style.display = 'block';
            if (!product.querySelector('.typeProduct').innerHTML.includes(type)) {
              product.style.display = 'none';
            }
          });
        }
      }
    });
  }

  _getCount = (arr) => {
    let counter = {};

    arr.forEach(function (obj) {
      counter[obj] = (counter[obj] || 0) + 1
    });

    return counter;
  }

}

const load = new Load();