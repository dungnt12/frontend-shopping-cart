class Cart {
  constructor() {
    this.init();
  }

  init() {
    this.cartDOM = qs('#table-cart');
    this._getCart();
    this._checkout();
  }

  _getCart = () => {
    const carts = localStorage.getItem('productsBuy');
    try {
      if (carts) {
        const cartPaser = JSON.parse(carts);
        const contentCart = cartPaser.map(({ description, id, images, price, title, type, count }) => (`
        <tr>
          <td class="p-4">
            <div class="media align-items-center">
              <img src="server/${images}" class="d-block ui-w-40 ui-bordered mr-4" alt="${title}">
              <div class="media-body">
                <a href="#" class="d-block text-dark">${title}</a>
                <small>
                  <span class="text-muted">${description}</span>
                  <span class="text-muted"><span class="badge badge-secondary">${type}</span></span>
                </small>
              </div>
            </div>
          </td>
          <td class="text-right font-weight-semibold align-middle p-4">$${price}</td>
          <td class="align-middle p-4">${count}</td>
          <td class="text-right font-weight-semibold align-middle p-4">$${price * count}</td>
          <td class="text-center align-middle px-0">
            <a id-remove="${id}" href="#" class="shop-tooltip close float-none text-danger remove-cart" title=""
              data-original-title="Remove">×</a></td>
          </tr>
        `));
        this.cartDOM.innerHTML = contentCart.join('');

        // get count
        let count = 0;
        cartPaser.forEach((item) => { count += item.price * item.count });
        qs('.total-price').innerHTML = `$${count}`;
        // remove cart
        document.querySelectorAll('.remove-cart').forEach((cart) => {
          const id = cart.getAttribute('id-remove');
          cart.onclick = (e) => {
            e.preventDefault();
            this._deleteCart(id);
          }
        });
      } else {
        this.cartDOM.innerHTML = 'Bạn chưa chọn mua sản phẩm nào?';
      }
    } catch (error) {
      console.log(error);
    }
  }

  _deleteCart(idProduct) {
    const carts = localStorage.getItem('productsBuy');
    const cartPaser = JSON.parse(carts);
    const indexCart = cartPaser.findIndex((item) => item.id === idProduct);
    cartPaser.splice(indexCart, 1);
    localStorage.setItem('productsBuy', JSON.stringify(cartPaser));
    this._getCart();
  }

  _checkout() {
    const checkout = qs('.checkout');
    checkout.onclick = () => {
      try {
        const carts = JSON.parse(localStorage.getItem('productsBuy'));
        const login = JSON.parse(localStorage.getItem('login'));
        $.ajax({
          type: "POST",
          url: "server/api/cart.php",
          data: {
            username: login.username,
            id: JSON.stringify(carts.map(({ id, count }) => ({ id, count }))),
          },
          success: () => {
            $('#cart').modal('hide');
            this.toast = _createToast('Succes', 'Đơn hàng đã được gửi đi', 'bg-success');
            document.body.appendChild(this.toast);
            $(`#${this.toast.id}`).toast('show');
          }
        });
      } catch (error) {
        console.log(error);
      }
    }
  }
}

const cart = new Cart();