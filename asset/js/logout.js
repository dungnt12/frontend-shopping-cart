class Logout {
  constructor() {
    this.init();
  }

  init() {
    const logoutBtn = qs('.logout');
    logoutBtn.onclick = () => {
      localStorage.clear();
      location.reload();
    }
  }
}

const logout = new Logout();