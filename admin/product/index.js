const qs = (el) => document.querySelector(el);
class Admin {
    data = [];
    dataProduct = [];
    dataUser = [];
    valueID ;
    idUser;
    constructor() {
        this.init();
    }

    init() {
        this._checkValid();
        this._getProduct();
        this._addProduct();
        this._getUser();
        this._editProduct();
        this._editUser();
        this._getTotal();
    }
    openCreateM(){
        $("#addNewProduct").modal()
        qs("#add-product").classList.remove("d-none");
        qs("#edit-product").classList.add("d-none");
        qs("#t-add").classList.remove("d-none");
        qs("#t-edit").classList.add("d-none");
        this.valueID='' ;
        qs('#type').value = '';
        qs('#title').value = '';
        qs('#description').value = '';
        qs('#price').value = '';
    }
    editProduct = (index) => {
        this.valueID = this.data[index].id;
        qs("#edit-product").classList.remove("d-none");
        qs("#add-product").classList.add("d-none");
        qs("#t-edit").classList.remove("d-none");
        qs("#t-add").classList.add("d-none");
        $("#addNewProduct").modal()
        qs('#type').value = this.data[index].type;
        qs('#title').value = this.data[index].title;
        qs('#description').value = this.data[index].description ;
        qs('#price').value = this.data[index].price;
    }

    editUser1 = (index) => {
        this.idUser = this.dataUser[index].id;
        $("#editUser").modal()
        qs('#username').value = this.dataUser[index].username;
        qs('#email').value = this.dataUser[index].email;
        qs('#password').value = this.dataUser[index].password;
        qs('#purchaseproduct').value = this.dataUser[index].purchaseproduct;
    }
    deleteProduct = (index) =>{
        this.valueID = this.data[index].id;
      var result = confirm("Bạn có chắc muốn xóa?");
      if (result) {
       
        $.ajax({
          type: "DELETE",
          url: `../../server/api/product.php?id=${this.valueID}`,
          processData: false,
          contentType: false,
          success: ($) => {
            const res = $ instanceof Array ? $ : [];
            const tbody = qs('#tbody-data');
            this.data = res;
            
            tbody.innerHTML = res.map(({ id, title, description, price, images, type }, index) => `
            <tr>
              <td>${id}</td>
              <td>${title}</td>
              <td>${description}</td>
              <td>${price}</td>
              <td><img style="width: 50px; height: 50px; object-fit: cover" src="../../server/${images}" class="rounded" alt="${title}"> </td>
              <td>${type}</td>
              <td>
                <div class="btn-group">
                  <button type="button" class="btn btn-primary"onclick=" admin.editProduct(${index})">Edit</button>
                  <button type="button" class="btn btn-danger" onclick=" admin.deleteProduct(${index})" id="delete-product" >Delete</button>
                </div> 
              </td>
            </tr>
          `).join('');
           
          }
        });
      }
       
    }
    deleteUser = (index) => {
        this.idUser = this.dataUser[index].id;
        var result = confirm("Bạn có chắc muốn xóa?");
        if(result){
          $.ajax({
            type: "DELETE",
            url: `../../server/api/user.php?id=${this.idUser}`,
            processData: false,
            contentType: false,
            success: (res) => {
              const tbody = qs('#tbody-data-user');
              this.dataUser = res;
              tbody.innerHTML = res.map(({ id, username, email, password, purchaseproduct }, index) => `
            <tr>
              <td>${id}</td>
              <td>${username}</td>
              <td>${email}</td>
              <td>${password}</td>
              <td>${purchaseproduct}</td>
              
              <td>
                <div class="btn-group">
                  <button type="button" class="btn btn-primary"onclick=" admin.editUser1(${index})">Edit</button>
                  <button type="button" class="btn btn-danger" onclick=" admin.deleteUser(${index})" >Delete</button>
                </div> 
              </td>
            </tr>
          `).join('');

            }
          });
        }
        
    }
    
    _getProduct = () => {
        $.ajax({
            type: "GET",
            url: "../../server/api/product.php",
            success: ($) => {
                const res = $ instanceof Array ? $ : [];
                const tbody = qs('#tbody-data');
                this.data = res;
                tbody.innerHTML = res.map(({ id, title, description, price, images, type },index) => `
            <tr>
              <td>${id}</td>
              <td>${title}</td>
              <td>${description}</td>
              <td>${price}</td>
              <td><img style="width: 50px; height: 50px; object-fit: cover" src="../../server/${images}" class="rounded" alt="${title}"> </td>
              <td>${type}</td>
              <td>
                <div class="btn-group">
                  <button type="button" class="btn btn-primary" onclick="admin.editProduct(${index})"  >Edit</button>
                  <button type="button" class="btn btn-danger" onclick="admin.deleteProduct(${index})"  >Delete</button>
                </div> 
              </td>
            </tr>
          `).join('');;

            }
        });

    }
    // them san pham
    _addProduct = () => {
        const addBtn = qs('#add-product');
        addBtn.onclick = () => {
            const formData = new FormData();
            formData.append('type', qs('#type').value);
            formData.append('title', qs('#title').value);
            formData.append('description', qs('#description').value);
            formData.append('price', qs('#price').value);
            formData.append('fileToUpload', qs('#images').files[0]);

            $.ajax({
                type: "POST",
                url: "../../server/api/product.php",
                data: formData,
                processData: false,
                contentType: false,
                success: (res) => {
                    const tbody = qs('#tbody-data');
                    tbody.innerHTML = res.map(({ id, title, description, price, images, type }, index) => `
                        <tr>
                        <td>${id}</td>
                        <td>${title}</td>
                        <td>${description}</td>
                        <td>${price}</td>
                        <td><img style="width: 50px; height: 50px; object-fit: cover" src="../../server/${images}" class="rounded" alt="${title}"> </td>
                        <td>${type}</td>
                        <td>
                            <div class="btn-group">
                            <button type="button" class="btn btn-primary" onclick="admin.editProduct(${index})"  >Edit</button>
                            <button type="button" class="btn btn-danger" onclick=" admin.deleteProduct(${index})" >Delete</button>
                            </div> 
                        </td>
                        </tr>
                    `).join('');
                    $('#addNewProduct').modal('hide');

                    // reset form data
                    qs('#type').value = '';
                    qs('#title').value = '';
                    qs('#description').value = '';
                    qs('#price').value = '';
                    qs('#images').value = '';
                }
            });
        }
    }
    //sua san pham
    _editProduct = () => {
        const addBtn = qs('#edit-product');
        addBtn.onclick = () => {
            const formData = new FormData();
            formData.append('id', this.valueID);
            formData.append('type', qs('#type').value);
            formData.append('title', qs('#title').value);
            formData.append('description', qs('#description').value);
            formData.append('price', qs('#price').value);
            formData.append('fileToUpload', qs('#images').files[0]);
         

            $.ajax({
                type: "POST",
                url: "../../server/api/product.php",
                data: formData,
                processData: false,
                contentType: false,
                success: (res) => {
                    const tbody = qs('#tbody-data');
                    tbody.innerHTML = res.map(({ id, title, description, price, images, type },index) => `
            <tr>
              <td>${id}</td>
              <td>${title}</td>
              <td>${description}</td>
              <td>${price}</td>
              <td><img style="width: 50px; height: 50px; object-fit: cover" src="../../server/${images}" class="rounded" alt="${title}"> </td>
              <td>${type}</td>
              <td>
                <div class="btn-group">
                  <button type="button" class="btn btn-primary"onclick=" admin.editProduct(${index})">Edit</button>
                  <button type="button" class="btn btn-danger" onclick=" admin.deleteProduct(${index})" >Delete</button>
                </div> 
              </td>
            </tr>
          `).join('');
                    $('#addNewProduct').modal('hide');

                    // reset form data
                    qs('#type').value = '';
                    qs('#title').value = '';
                    qs('#description').value = '';
                    qs('#price').value = '';
                    qs('#images').files = [];
                }
            });
        }
    }
    
    _getUser = () => {
        $.ajax({
            type: "GET",
            url: "../../server/api/user.php",
            success: (res) => {
                const tbody = qs('#tbody-data-user');
                this.dataUser = res;
                tbody.innerHTML = res.map(({ id, username, email, password, purchaseproduct },index) => `
                  <tr>
                    <td>${id}</td>
                    <td>${username}</td>
                    <td>${email}</td>
                    <td>${password}</td>
                    <td>${purchaseproduct}</td>
                    <td>
                      <div class="btn-group">
                        <button type="button" class="btn btn-primary" onclick ="admin.editUser1(${index})" >Edit</button>
                        <button type="button" class="btn btn-danger" onclick ="admin.deleteUser(${index})">Delete</button>
                      </div> 
                    </td>
                  </tr>
                `).join('');
            }
        });

        

    }
    _editUser = () => {
        const addBtn = qs('#edit-user');
        addBtn.onclick = () => {
            const formData = new FormData();
            formData.append('id', this.idUser);
            formData.append('username', qs('#username').value);
            formData.append('email', qs('#email').value);
            formData.append('password', qs('#password').value);
            formData.append('purchaseproduct', qs('#purchaseproduct').value);
            formData.append('type', 'edit');

            $.ajax({
                type: "POST",
                url: "../../server/api/user.php",
                data: formData,
                processData: false,
                contentType: false,
                success: (res) => {
                    const tbody = qs('#tbody-data-user');
                    this.dataUser = res;
                    tbody.innerHTML = res.map(({ id, username, email, password, purchaseproduct }, index) => `
                    <tr>
                        <td>${id}</td>
                        <td>${username}</td>
                        <td>${email}</td>
                        <td>${password}</td>
                        <td>${purchaseproduct}</td>
                        <td>
                        <div class="btn-group">
                            <button type="button" class="btn btn-primary" onclick ="admin.editUser1(${index})">Edit</button>
                            <button type="button" class="btn btn-danger" onclick ="admin.deleteUser(${index})" >Delete</button>
                        </div> 
                        </td>
                    </tr>
                    `).join('');
                    $('#editUser').modal('hide');

                    // reset form data
                    qs('#username').value = '';
                    qs('#email').value = '';
                    qs('#password').value = '';
                    qs('#purchaseproduct').value = '';
                }
            });
        }
    }
    _getTotal = () =>{
      $.ajax({
        type: "GET",
        url: "../../server/api/total.php",
        success: (res) => {
          const tbody = qs('#tbody-total');
          this.dataUser = res;
          tbody.innerHTML = res.map(({ id, username, purchaseproduct }) => `
          <tr>
            <td>${id}</td>
            <td>${username}</td>
            <td>${genProduct(JSON.parse(purchaseproduct))}</td>

          </tr>
        `).join('');
        }
      });
      function genProduct(a) {
        
        return a.map((item) => `<div>${item.id}: ${item.count}</div>`).join('');
      };
    }
    

    _checkValid = () => {
        const dataLocalstorage = localStorage.getItem('login');
        try {
            const dataPaser = JSON.parse(dataLocalstorage);
            if (!dataPaser || dataPaser.role !== 'admin') {
                window.location.replace("404");
            }
        } catch (error) {
            console.log(error);
        }
    }
}

const admin = new Admin();